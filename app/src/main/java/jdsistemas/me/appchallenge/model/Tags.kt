package jdsistemas.me.appchallenge.model

import com.orm.SugarRecord
import org.parceler.Parcel
import java.io.Serializable

/**
 * Created on data 27/07/18.
 */
@Parcel(Parcel.Serialization.BEAN)
class Tags(val id: Int = 0, val label: String): SugarRecord(), Serializable {

//    private val id: Long = 0
//    private val label: String
}
